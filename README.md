# CarCar

Team:

-   Leo Shon - Sales microservice
-   Matthew Young - Services microservice

## Setup

1. Have Docker Desktop open and running
2. Open terminal and run the following commands:
   <br>
   `git clone https://gitlab.com/matthew.young777/project-beta`
   <br>
   `cd project-beta`
   <br>
   `docker volume create beta-data`
   <br>
   `docker-compose build`
   <br>
   `docker-compose up`

3. Open browser to http://localhost:3000/ and watch your terminal. It will be running when you see: <br>`You can now view app in the browser.`

## Design

## ![Context Map](design-diagram.png "Diagram")

## Services microservice

_Manages service appointments for the dealership_

We use data polling to gather information about Automobiles in Inventory, and create "mock" automobiles in the form of AutoVO's to limit the interaction between microservices, and increase the autonomy of Services.

#### Models

##### Technician

Represents a technician with a name, and employee number. Each appointment requires a technician.

##### AutoVO

A "value object" model to mirror the Automobile model in the Inventory microservice. Contains only the VIN of an automobile, which is all that is needed to identify an auto between inventory and services. Allows the microservice to track automobiles without leaving its bounded context.

##### Appointment

1. VIN - the VIN of the automobile
2. Customer Name
3. Date and Time of appointment
4. Assigned Technician
5. Reason for service
6. VIP treatment (for vehicles purchased from the dealer)
7. Finished status (to track completed appointments)

#### How to use

> Appointments must have an assigned technician, so we'll need to create some first

##### 1. Creating a technician

-   Create at least one Technician on the "New Technician" page. This can be acccessed through the "Services" dropdown menu in the Nav bar. **Technicians must have a unique employee number**
    <br>

##### 2. Creating a service appointment

-   Create an Service appointment on the "New Appointment page" from the "Services" dropdown menu. Enter the VIN of your vehicle, your name, select the date and time for your appointment, a technician, and enter the reason for your service appointment.
    <br>

##### 3. Viewing/editing appointments

-   The List Appointments Page in the "Services" dropdown menu allows concierge to view all scheduled appointments that haven't been finished yet, as well as cancel or mark any as complete. Once cancelled or completed, the appointment will no longer show up in upcoming appointments.
-   On the Service History page from the same dropdown menu, you can enter a VIN, and see the entire list of appointments associated with that automobile.

## Sales microservice

Salesperson

-   started off by making a salesperson model with name and number as it's parameters with the appropriate view functions, urls, and encoders.
    -   if you click on the dropdown menu you can see the form to create a salesperson with the given parameters
        -   the form draws data from the url created for the salesperson and requests a post method handled in views.
-   the upon creating the form you will see a success message noting that the salesperson has been hired!

Customer

-   Similar backend methods are used to create a customer as well.
    -   when finishing the form you will also see a success message showing a new customer has been added

Sale

-   The sale model was a bit more trickier. 3 of 4 fields are foreign key fields refering to a different model and 1 of those 3 is going to be pulling data from the inventory microservice by polling.
    -   I had to create a new VO model for automobile so that the poller can copy the data from the automobile model in inventory into the automobilevo model in sales
    -   once I got the polling to work everything was smooth sailing!
-   The form uses three dropdowns and a field for price.
    -   automobile dropdown lists all the VIN numbers since that is the only way to identify each specific car in the inventory.
    -   salesperson and customer are drawn from the informatin we created in the previous two models.
    -   if the sale is created successfully you will see a message congratulating you on a new sale!
-   Note that once you make a sale, the specific car will be deleted off in the inventory so when you look down the dropdown menu or the list page for automobiles the car you just sold won't be there.

Saleslist

-   this page will list all sales form each salesperson, their number, the vin # for the car they sold, the customer, and for how much.
    -   it is requesting a GET method to the url of sales to get all the data.

Salesperson's Sale History

-   When you enter the page it will list all the sales like the last page but there is a dropdown menu listing all salespersons. Once clicking a specific salesperson you will be able to see only the sales that the specific salesperson you chose made.
    -   this is handled through the handlechange function in the js file
