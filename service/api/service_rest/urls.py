from django.urls import path

from .views import api_list_appointments, api_appointment_detail, api_list_technicians, api_technician_detail, api_list_autos

urlpatterns = [
    path("appointments/", api_list_appointments, name="list_appointments"),
    path("appointments/<str:auto_vin>/", api_list_appointments,
         name="appointment_vin"),
    path("appointments/edit/<int:pk>/",
         api_appointment_detail, name="edit_appointment"),
    path("technicians/", api_list_technicians, name="list_technicians"),
    path("technicians/<int:pk>/", api_technician_detail, name="technician_detail"),
    path("autos/", api_list_autos, name="list_autos"),
]
