# Generated by Django 4.0.3 on 2022-09-14 22:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0007_appointment_finished'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='finished',
            field=models.BooleanField(),
        ),
    ]
