from django.db import models
from django.urls import reverse
# Create your models here.


class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.IntegerField(unique=True)

    def __str__(self):
        return self.name


class AutoVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)
    import_href = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})


class Appointment(models.Model):
    auto_vin = models.CharField(max_length=200)
    customer_name = models.CharField(max_length=200)
    time = models.DateTimeField()
    technician = models.ForeignKey(
        Technician, related_name="appointments", on_delete=models.CASCADE)
    reason = models.CharField(max_length=200)
    vip = models.BooleanField()
    finished = models.BooleanField(default=False)

    def __str__(self):
        return (f"{self.reason} for {self.customer_name}")

    def finish(self):
        self.finished = True
