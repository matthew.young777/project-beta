from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Technician, AutoVO, Appointment


class AutoVODetailEncoder(ModelEncoder):
    model = AutoVO
    properties = ["vin", "import_href"]


class AutoVOListEncoder(ModelEncoder):
    model = AutoVO
    properties = ["vin"]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["name", "employee_number"]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = ["name", "employee_number"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "customer_name",
        "time", "reason", "auto_vin", "vip", "id", "finished"
    ]

    def get_extra_data(self, o):
        return {
            "technician": o.technician.name,
        }


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "auto_vin", "customer_name",
        "time", "technician", "reason", "vip"
    ]

    encoders = {
        "technician": TechnicianDetailEncoder(),
    }


@require_http_methods(["GET"])
def api_list_autos(request):
    autos = AutoVO.objects.all()
    return JsonResponse(
        {"autos": autos},
        encoder=AutoVOListEncoder,
    )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request, auto_vin=None):
    if request.method == "GET":
        if auto_vin is not None:
            appointments = Appointment.objects.filter(
                auto_vin=auto_vin)
        else:
            appointments = Appointment.objects.filter(finished=False)
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            vin_number = content["auto_vin"]
            auto = AutoVO.objects.get(vin=vin_number)
            content["vip"] = True

        except AutoVO.DoesNotExist:
            content["vip"] = False
        try:
            employee_number = content["technician"]
            technician = Technician.objects.get(
                employee_number=employee_number)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee number"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_appointment_detail(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        appointment = Appointment.objects.get(id=pk)
        appointment.finish()
        appointment.save()
        return JsonResponse({
            "finished": appointment.finished,
        })
    else:
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@ require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )


@ require_http_methods(["GET", "DELETE"])
def api_technician_detail(request, pk):
    if request.method == "GET":
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
