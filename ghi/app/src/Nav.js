import { NavLink } from "react-router-dom";

function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-success">
            <div className="container-fluid">
                <NavLink className="navbar-brand" to="/">
                    CarCar
                </NavLink>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div
                    className="collapse navbar-collapse"
                    id="navbarSupportedContent"
                >
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <NavLink
                                className="nav-link"
                                to="/manufacturers/new"
                            >
                                New Manufacturer
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/manufacturers">
                                Manufacturers
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/models/new">
                                New Vehicle Model
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/models">
                                Vehicle Models
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="automobiles">
                                Automobiles
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="automobiles/new">
                                New Automobile
                            </NavLink>
                        </li>
                        <li className="nav-item dropdown">
                            <a
                                className="nav-link dropdown-toggle"
                                href="#"
                                role="button"
                                data-bs-toggle="dropdown"
                                aria-expanded="false"
                            >
                                Sales
                            </a>
                            <ul className="dropdown-menu">
                                <li>
                                    <NavLink
                                        className="nav-item dropdown-item"
                                        to="salesperson/new"
                                    >
                                        New Salesperson
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink
                                        className="nav-item dropdown-item"
                                        to="customer/new"
                                    >
                                        New Customer
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink
                                        className="nav-item dropdown-item"
                                        to="sales/new"
                                    >
                                        New Sales
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink
                                        className="nav-item dropdown-item"
                                        to="sales"
                                    >
                                        Sales List
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink
                                        className="nav-item dropdown-item"
                                        to="saleshistory"
                                    >
                                        Salesperson's Sale History
                                    </NavLink>
                                </li>
                            </ul>
                        </li>
                        <li className="nav-item dropdown">
                            <a
                                className="nav-link dropdown-toggle"
                                href="#"
                                role="button"
                                data-bs-toggle="dropdown"
                                aria-expanded="false"
                            >
                                Services
                            </a>
                            <ul className="dropdown-menu">
                                <li>
                                    <NavLink
                                        className="nav-item dropdown-item"
                                        to="technicians/new"
                                    >
                                        New Technician
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink
                                        className="nav-item dropdown-item"
                                        to="new_appointment"
                                    >
                                        New Appointment
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink
                                        className="nav-item dropdown-item"
                                        to="appointments"
                                    >
                                        List Appointments
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink
                                        className="nav-item dropdown-item"
                                        to="appointments_history"
                                    >
                                        Service History
                                    </NavLink>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}

export default Nav;
