import React from "react";

class SalesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            automobile: "",
            salesperson: "",
            customer: "",
            price: "",
            sales: [],
        };
    }
    async componentDidMount() {
        const url = "http://localhost:8090/api/sales/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            this.setState({ sales: data.sales });
        }
    }
    render() {
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Name</th>
                        <th>Salesperson Number</th>
                        <th>Automobile VIN</th>
                        <th>Customer</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.sales.map((sale) => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson}</td>
                                <td>{sale.number}</td>
                                <td>{sale.automobile}</td>
                                <td>{sale.customer}</td>
                                <td>{sale.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        );
    }
}

export default SalesList;
