import React from "react";

class VehicleList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            pictureurl: "",
            manufacturers: "",
            models: [],
        };
    }
    async componentDidMount() {
        const url = "http://localhost:8100/api/models/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            this.setState({ models: data.models });
        }
    }
    render() {
        return (
            <>
                <br></br>
                <h2>Vehicle Models</h2>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Picture</th>
                            <th>Manufacturer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.models.map((model) => {
                            return (
                                <tr key={model.id}>
                                    <td>{model.name}</td>
                                    <td>
                                        <img
                                            alt="picture"
                                            src={model.picture_url}
                                            className="w-25 p-3"
                                        />
                                    </td>
                                    <td>{model.manufacturer.name}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </>
        );
    }
}

export default VehicleList;
