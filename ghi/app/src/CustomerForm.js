import React from "react";

class CustomerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            address: "",
            phonenumber: "",
            newcustomer: false,
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handlePhonenumberChange = this.handlePhonenumberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }
    handleAddressChange(event) {
        const value = event.target.value;
        this.setState({ address: value });
    }
    handlePhonenumberChange(event) {
        const value = event.target.value;
        this.setState({ phonenumber: value });
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.newcustomer;
        console.log(data);

        const CustomerUrl = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(CustomerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            console.log(newCustomer);

            const cleared = {
                name: "",
                address: "",
                phonenumber: "",
                newcustomer: true,
            };
            this.setState(cleared);
            await setTimeout(() => {
                window.location.reload();
            }, 1000);
        }
    }
    render() {
        let messageClasses = "alert alert-success d-none mb-0";
        // let formClasses = "";
        if (this.state.newcustomer) {
            messageClasses = "alert alert-success mb-0";
            // formClasses = "d-none";
        }
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Customer</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-customer-form"
                        >
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleNameChange}
                                    value={this.state.name}
                                    placeholder="Name"
                                    required
                                    type="text"
                                    name="name"
                                    id="name"
                                    className="form-control"
                                />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleAddressChange}
                                    value={this.state.address}
                                    placeholder="Address"
                                    required
                                    type="text"
                                    name="address"
                                    id="address"
                                    className="form-control"
                                />
                                <label htmlFor="name">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handlePhonenumberChange}
                                    value={this.state.phonenumber}
                                    placeholder="phonenumber"
                                    required
                                    type="text"
                                    name="phonenumber"
                                    id="phonenumber"
                                    className="form-control"
                                />
                                <label htmlFor="phonenumber">
                                    Phone Number
                                </label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
                <div className={messageClasses} id="success-message">
                    New Customer Added!
                </div>
            </div>
        );
    }
}
export default CustomerForm;
