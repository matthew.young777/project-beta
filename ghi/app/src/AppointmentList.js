import React from "react";

class AppointmentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appointments: [],
        };
        this.handleCancel = this.handleCancel.bind(this);
        this.handleFinish = this.handleFinish.bind(this);
    }

    async componentDidMount() {
        //Loading Appointments
        const appointmentsURL = "http://localhost:8080/api/appointments/";
        const response = await fetch(appointmentsURL);

        if (response.ok) {
            const data = await response.json();
            this.setState({ appointments: data.appointments });
        }
    }

    async handleCancel(event) {
        const value = event.target.value;
        const appointmentsURL = `http://localhost:8080/api/appointments/edit/${value}/`;
        const fetchConfig = {
            method: "delete",
        };

        const response = await fetch(appointmentsURL, fetchConfig);
        const wasDeleted = await response.json();
        console.log(wasDeleted);
        if (response.ok) {
            this.componentDidMount();
        }
    }

    async handleFinish(event) {
        const value = event.target.value;
        const appointmentsURL = `http://localhost:8080/api/appointments/edit/${value}/`;
        const fetchConfig = {
            method: "put",
        };

        const response = await fetch(appointmentsURL, fetchConfig);
        const wasFinished = await response.json();
        console.log(wasFinished);
        if (response.ok) {
            this.componentDidMount();
        }
    }

    render() {
        if (this.state.appointments === []) {
        }
        return (
            <>
                <br></br>
                <h1>Upcoming Appointments</h1>
                <table className="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>VIP status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.map((appointment) => {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.auto_vin}</td>
                                    <td>{appointment.customer_name}</td>
                                    <td>
                                        {new Date(
                                            appointment.time
                                        ).toLocaleDateString()}
                                    </td>
                                    <td>
                                        {new Date(
                                            appointment.time
                                        ).toLocaleTimeString([], {
                                            hour: "2-digit",
                                            minute: "2-digit",
                                        })}
                                    </td>
                                    <td>{appointment.technician}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.vip ? "yes" : "no"}</td>
                                    <td>
                                        <button
                                            onClick={this.handleCancel}
                                            value={appointment.id}
                                            className="btn btn-outline-danger"
                                        >
                                            Cancel
                                        </button>
                                    </td>
                                    <td>
                                        <button
                                            onClick={this.handleFinish}
                                            value={appointment.id}
                                            className="btn btn-outline-success"
                                        >
                                            Finish
                                        </button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </>
        );
    }
}
export default AppointmentList;
