import React from "react";

class AppointmentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            customer_name: "",
            auto_vin: "",
            time: "",
            technician: "",
            technicians: [],
            reason: "",
            success: false,
        };
        this.handleChangeCustomerName =
            this.handleChangeCustomerName.bind(this);
        this.handleChangeVin = this.handleChangeVin.bind(this);
        this.handleChangeTechnician = this.handleChangeTechnician.bind(this);
        this.handleChangeReason = this.handleChangeReason.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeCustomerName(event) {
        const value = event.target.value;
        this.setState({ customer_name: value });
    }

    handleChangeVin(event) {
        const value = event.target.value;
        this.setState({ auto_vin: value });
    }
    handleChangeTechnician(event) {
        const value = event.target.value;
        this.setState({ technician: value });
    }
    handleChangeReason(event) {
        const value = event.target.value;
        this.setState({ reason: value });
    }
    handleChangeDate(event) {
        const value = event.target.value;
        this.setState({ time: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.success;
        delete data.technicians;
        const AppUrl = `http://localhost:8080/api/appointments/${data["auto_vin"]}/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(AppUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            console.log(newAppointment);
            this.setState({
                customer_name: "",
                auto_vin: "",
                time: "",
                technician: "",
                technicians: [],
                reason: "",
                success: true,
            });
            await setTimeout(() => {
                // this.componentDidMount();
                window.location.reload();
            }, 2000);
        }
    }

    async componentDidMount() {
        // Loading technicians
        const techUrl = "http://localhost:8080/api/technicians/";
        const techResponse = await fetch(techUrl);

        if (techResponse.ok) {
            const techData = await techResponse.json();
            this.setState({ technicians: techData.technicians });
        }
    }

    render() {
        let messageClasses = "alert alert-success d-none mb-0";
        if (this.state.success) {
            messageClasses = "alert alert-success mb-0";
        }
        return (
            <div className="row">
                <div className={messageClasses} id="success-message">
                    Appointment Scheduled!
                </div>
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Service Appointment</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-appointment-form"
                        >
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleChangeVin}
                                    value={this.state.auto_vin}
                                    placeholder="Vehicle VIN"
                                    required
                                    type="text"
                                    name="auto_vin"
                                    id="auto_vin"
                                    className="form-control"
                                />
                                <label htmlFor="auto_vin">Vehicle VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleChangeCustomerName}
                                    value={this.state.customer_name}
                                    placeholder="Customer Name"
                                    required
                                    type="text"
                                    name="customer_name"
                                    id="customer_name"
                                    className="form-control"
                                />
                                <label htmlFor="customer_name">
                                    Customer Name
                                </label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleChangeDate}
                                    value={this.state.time}
                                    placeholder="Date"
                                    required
                                    type="datetime-local"
                                    name="time"
                                    id="time"
                                    className="form-control"
                                />
                                <label htmlFor="time">Date</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    onChange={this.handleChangeTechnician}
                                    value={this.state.technician}
                                    required
                                    name="technician"
                                    id="technician"
                                    className="form-select"
                                >
                                    <option value="">Select Technician</option>
                                    {this.state.technicians.map(
                                        (technician) => {
                                            return (
                                                <option
                                                    key={
                                                        technician.employee_number
                                                    }
                                                    value={
                                                        technician.employee_number
                                                    }
                                                >
                                                    {technician.name}
                                                </option>
                                            );
                                        }
                                    )}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleChangeReason}
                                    value={this.state.reason}
                                    placeholder="Reason"
                                    required
                                    type="text"
                                    name="reason"
                                    id="reason"
                                    className="form-control"
                                />
                                <label htmlFor="reason">Reason</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default AppointmentForm;
