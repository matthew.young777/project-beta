import React from "react";

class AutoList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            autos: [],
        };
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/automobiles/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            this.setState({ autos: data.autos });
        }
    }

    render() {
        if (this.state.autos === []) {
        }
        return (
            <>
                <br></br>
                <h2>Automobiles in Inventory</h2>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Color</th>
                            <th>Model</th>
                            <th>Manufacturer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.autos.map((auto) => {
                            return (
                                <tr key={auto.id}>
                                    <td>{auto.vin}</td>
                                    <td>{auto.color}</td>
                                    <td>{auto.model.name}</td>
                                    <td>{auto.model.manufacturer.name}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </>
        );
    }
}
export default AutoList;
