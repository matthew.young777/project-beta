import React from "react";

class SalespersonForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            number: "",
            newperson: false,
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleNumberChange = this.handleNumberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }
    handleNumberChange(event) {
        const value = event.target.value;
        this.setState({ number: value });
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.newperson;
        console.log(data);

        const SalespersonUrl = "http://localhost:8090/api/salespersons/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(SalespersonUrl, fetchConfig);
        if (response.ok) {
            const newSalesperson = await response.json();
            console.log(newSalesperson);

            const cleared = {
                name: "",
                number: "",
                newperson: true,
            };
            this.setState(cleared);
            await setTimeout(() => {
                window.location.reload();
            }, 1000);
        }
    }
    render() {
        let messageClasses = "alert alert-success d-none mb-0";
        // let formClasses = "";
        if (this.state.newperson) {
            messageClasses = "alert alert-success mb-0";
            // formClasses = "d-none";
        }
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Salesperson</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-salesperson-form"
                        >
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleNameChange}
                                    value={this.state.name}
                                    placeholder="Name"
                                    required
                                    type="text"
                                    name="name"
                                    id="name"
                                    className="form-control"
                                />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleNumberChange}
                                    value={this.state.number}
                                    placeholder="Number"
                                    required
                                    type="text"
                                    name="number"
                                    id="number"
                                    className="form-control"
                                />
                                <label htmlFor="number">Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
                <div className={messageClasses} id="success-message">
                    Hired a new salesperson!
                </div>
            </div>
        );
    }
}
export default SalespersonForm;
