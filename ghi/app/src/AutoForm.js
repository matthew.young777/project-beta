import React from "react";

class AutoForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            color: "",
            year: "",
            vin: "",
            models: [],
            model_id: "",
            newauto: false,
        };
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangeYear = this.handleChangeYear.bind(this);
        this.handleChangeVin = this.handleChangeVin.bind(this);
        this.handleChangeModel = this.handleChangeModel.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeColor(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }
    handleChangeYear(event) {
        const value = event.target.value;
        this.setState({ year: parseInt(value) });
    }
    handleChangeVin(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }
    handleChangeModel(event) {
        const value = event.target.value;
        this.setState({ model_id: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.models;
        delete data.newauto;
        console.log(data);
        const autoURL = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(autoURL, fetchConfig);
        if (response.ok) {
            const newAuto = await response.json();
            console.log(newAuto);
            this.setState({
                color: "",
                year: "",
                vin: "",
                models: [],
                model_id: "",
                newauto: true,
            });
            // this.setState(cleared);
            await setTimeout(() => {
                window.location.reload();
            }, 1000);
        }
    }
    async componentDidMount() {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ models: data.models });
        }
    }

    render() {
        let messageClasses = "alert alert-success d-none mb-0";
        // let formClasses = "";
        if (this.state.newauto) {
            messageClasses = "alert alert-success mb-0";
            // formClasses = "d-none";
        }
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add an automobile to inventory</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-auto-form"
                        >
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleChangeColor}
                                    value={this.state.color}
                                    placeholder="Color"
                                    required
                                    type="text"
                                    name="color"
                                    id="color"
                                    className="form-control"
                                />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleChangeYear}
                                    value={this.state.year}
                                    placeholder="Year"
                                    required
                                    type="number"
                                    name="year"
                                    id="year"
                                    className="form-control"
                                />
                                <label htmlFor="year">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    maxLength={17}
                                    onChange={this.handleChangeVin}
                                    value={this.state.vin}
                                    placeholder="VIN"
                                    required
                                    type="text"
                                    name="vin"
                                    id="vin"
                                    className="form-control"
                                />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    onChange={this.handleChangeModel}
                                    value={this.state.model_id}
                                    required
                                    name="model"
                                    id="model"
                                    className="form-select"
                                >
                                    <option value="">Choose a model</option>
                                    {this.state.models.map((model) => {
                                        return (
                                            <option
                                                key={model.id}
                                                value={model.id}
                                            >
                                                {model.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
                <div className={messageClasses} id="success-message">
                    New Automobile added to inventory!
                </div>
            </div>
        );
    }
}

export default AutoForm;
