import React from "react";

class ManufacturerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            newmanu: false,
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.newmanu;
        console.log(data);

        const ManufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(ManufacturerUrl, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();
            console.log(newManufacturer);

            const cleared = {
                name: "",
                newmanu: true,
            };
            this.setState(cleared);
            await setTimeout(() => {
                window.location.reload();
            }, 1000);
        }
    }
    render() {
        let messageClasses = "alert alert-success d-none mb-0";
        // let formClasses = "";
        if (this.state.newmanu) {
            messageClasses = "alert alert-success mb-0";
            // formClasses = "d-none";
        }
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Manufacturer</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-location-form"
                        >
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleNameChange}
                                    value={this.state.name}
                                    placeholder="Name"
                                    required
                                    type="text"
                                    name="name"
                                    id="name"
                                    className="form-control"
                                />
                                <label htmlFor="name">Name</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
                <div className={messageClasses} id="success-message">
                    New Manufacturer Created!
                </div>
            </div>
        );
    }
}

export default ManufacturerForm;
