import React from "react";

class SalespersonHistoryList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            automobile: "",
            salesperson: "",
            customer: "",
            price: "",
            salespersons: [],
            sales: [],
        };
        // this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeSalesperson = this.handleChangeSalesperson.bind(this);
    }
    async handleChangeSalesperson(event) {
        const value = event.target.value;
        this.setState({ salesperson: value });
        const salesUrl = `http://localhost:8090/api/sales/${value}`;
        const salesResponse = await fetch(salesUrl);
        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            this.setState({ sales: salesData.sales });
        }
    }
    // async handleSubmit(event) {
    //     event.preventDefault();
    //     const data = { ...this.state };
    //     delete data.salespersons;
    //     console.log(data);

    //     const SaleUrl = "http://localhost:8090/api/sales/";
    //     const fetchConfig = {
    //         method: "get",
    //         body: JSON.stringify(data),
    //         headers: {
    //             "Content-Type": "application/json",
    //         },
    //     };
    //     const response = await fetch(SaleUrl, fetchConfig);
    //     if (response.ok) {
    //     }
    // }
    async componentDidMount() {
        const autoUrl = "http://localhost:8100/api/automobiles/";
        const salesPersonUrl = "http://localhost:8090/api/salespersons/";
        const customerUrl = "http://localhost:8090/api/customers/";
        const salesUrl = "http://localhost:8090/api/sales/";

        const autoResponse = await fetch(autoUrl);
        const salesPersonResponse = await fetch(salesPersonUrl);
        const customerResponse = await fetch(customerUrl);
        const salesResponse = await fetch(salesUrl);

        if (
            autoResponse.ok &&
            salesPersonResponse.ok &&
            customerResponse.ok &&
            salesResponse.ok
        ) {
            const autoData = await autoResponse.json();
            const salesPersonData = await salesPersonResponse.json();
            const customerData = await customerResponse.json();
            const salesData = await salesResponse.json();
            console.log("automobile", autoData);
            console.log("customer", customerData);
            console.log("salesperson", salesPersonData);
            this.setState({ automobiles: autoData.autos });
            this.setState({ salespersons: salesPersonData.salespersons });
            this.setState({ customers: customerData.customers });
            this.setState({ sales: salesData.sales });
        }
    }
    render() {
        return (
            <div className="container">
                <div className="mb-3">
                    <form
                        // onSubmit={this.handleSubmit}
                        id="list-specific-salesperson-history"
                    >
                        <select
                            onChange={this.handleChangeSalesperson}
                            value={this.state.salesperson}
                            required
                            name="salesperson"
                            id="salesperson"
                            className="form-select"
                        >
                            <option value="">Choose a Salesperson</option>
                            {this.state.salespersons.map((salesperson) => {
                                return (
                                    <option
                                        key={salesperson.id}
                                        value={salesperson.id}
                                    >
                                        {salesperson.name}
                                    </option>
                                );
                            })}
                        </select>
                        {/* <button className="btn btn-primary">Search</button> */}
                    </form>
                </div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Salesperson Name</th>
                            <th>Customer</th>
                            <th>Automobile VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.sales.map((sale) => {
                            return (
                                <tr key={sale.id}>
                                    <td>{sale.salesperson}</td>
                                    <td>{sale.customer}</td>
                                    <td>{sale.automobile}</td>
                                    <td>{sale.price}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default SalespersonHistoryList;
