import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ManufacturerList from "./ManufacturerList";
import ManufacturerForm from "./ManufacturerForm";
import AutoList from "./AutoList";
import AutoForm from "./AutoForm";
import VehicleList from "./VehicleList";
import VehicleForm from "./VehicleForm";
import TechnicianForm from "./TechnicianForm";
import AppointmentForm from "./AppointmentForm";
import AppointmentList from "./AppointmentList";
import ServiceHistory from "./ServiceHistory";
import SalespersonForm from "./SalespersonForm";
import CustomerForm from "./CustomerForm";
import SalesForm from "./SalesForm";
import SalesList from "./SalesList";
import SalespersonHistoryList from "./SalespersonHistoryList";

function App() {
    return (
        <BrowserRouter>
            <Nav />
            <div className="container">
                <Routes>
                    <Route path="/" element={<MainPage />} />
                    <Route
                        path="manufacturers"
                        element={<ManufacturerList />}
                    />
                    <Route
                        path="manufacturers/new"
                        element={<ManufacturerForm />}
                    />
                    <Route path="automobiles" element={<AutoList />} />
                    <Route path="automobiles/new" element={<AutoForm />} />
                    <Route path="models" element={<VehicleList />} />
                    <Route path="models/new" element={<VehicleForm />} />
                    <Route
                        path="salesperson/new"
                        element={<SalespersonForm />}
                    />
                    <Route path="customer/new" element={<CustomerForm />} />
                    <Route path="sales/new" element={<SalesForm />} />
                    <Route path="sales" element={<SalesList />} />
                    <Route
                        path="saleshistory"
                        element={<SalespersonHistoryList />}
                    />
                    <Route
                        path="technicians/new"
                        element={<TechnicianForm />}
                    />
                    <Route
                        path="new_appointment"
                        element={<AppointmentForm />}
                    />
                    <Route path="appointments" element={<AppointmentList />} />
                    <Route
                        path="appointments_history"
                        element={<ServiceHistory />}
                    />
                </Routes>
            </div>
        </BrowserRouter>
    );
}

export default App;
