import React from "react";

class SalesForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            automobiles: [],
            salespersons: [],
            customers: [],
            price: "",
            sold: false,
        };
        this.handleChangeAutomobile = this.handleChangeAutomobile.bind(this);
        this.handleChangeSalesperson = this.handleChangeSalesperson.bind(this);
        this.handleChangeCustomer = this.handleChangeCustomer.bind(this);
        this.handleChangePrice = this.handleChangePrice.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChangeAutomobile(event) {
        const value = event.target.value;
        this.setState({ automobile: value });
    }
    handleChangeSalesperson(event) {
        const value = event.target.value;
        this.setState({ salesperson: value });
    }
    handleChangeCustomer(event) {
        const value = event.target.value;
        this.setState({ customer: value });
    }
    handleChangePrice(event) {
        const value = event.target.value;
        this.setState({ price: value });
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.automobiles;
        delete data.salespersons;
        delete data.customers;
        delete data.sold;
        console.log(data);

        const salepostUrl = "http://localhost:8090/api/sales/";
        const salepostfetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        console.log(this.state.automobile);
        const autodeleteUrl = `http://localhost:8100/api/automobiles/${this.state.automobile}`;
        const autodeletefetchConfig = {
            method: "delete",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const salepostresponse = await fetch(salepostUrl, salepostfetchConfig);
        const autodeleteresponse = await fetch(
            autodeleteUrl,
            autodeletefetchConfig
        );
        if (salepostresponse.ok && autodeleteresponse.ok) {
            const newSale = await salepostresponse.json();
            const deleteAuto = await autodeleteresponse.json();
            console.log(newSale);
            console.log(deleteAuto);
            const cleared = {
                automobile: "",
                salesperson: "",
                customer: "",
                price: "",
                sold: true,
            };
            this.setState(cleared);
            await setTimeout(() => {
                window.location.reload();
            }, 2000);
            // window.location.reload();
        }
    }
    async componentDidMount() {
        const autoUrl = "http://localhost:8100/api/automobiles/";
        const salesPersonUrl = "http://localhost:8090/api/salespersons/";
        const customerUrl = "http://localhost:8090/api/customers/";

        const autoResponse = await fetch(autoUrl);
        const salesPersonResponse = await fetch(salesPersonUrl);
        const customerResponse = await fetch(customerUrl);

        if (autoResponse.ok && salesPersonResponse.ok && customerResponse.ok) {
            const autoData = await autoResponse.json();
            const salesPersonData = await salesPersonResponse.json();
            const customerData = await customerResponse.json();
            console.log("automobile", autoData);
            console.log("customer", customerData);
            console.log("salesperson", salesPersonData);
            this.setState({ automobiles: autoData.autos });
            this.setState({ salespersons: salesPersonData.salespersons });
            this.setState({ customers: customerData.customers });
        }
    }
    // async componentDidMount1() {
    //     console.log("salesperson");
    //     const url = "http://localhost:8090/api/salespersons/";
    //     const response = await fetch(url);

    //     if (response.ok) {
    //         const data = await response.json();
    //         console.log("salesperson", data);
    //         this.setState({ salespersons: data.salespersons });
    //     }
    // }
    // async componentDidMount2() {
    //     const url = "http://localhost:8090/api/customers/";

    //     const response = await fetch(url);

    //     if (response.ok) {
    //         const data = await response.json();
    //         console.log('customer', data);
    //         this.setState({ customers: data.customers });
    //     }
    // }
    render() {
        let messageClasses = "alert alert-success d-none mb-0";
        // let formClasses = "";
        if (this.state.sold) {
            messageClasses = "alert alert-success mb-0";
            // formClasses = "d-none";
        }
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Sale</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-sale-form"
                        >
                            <div className="mb-3">
                                <select
                                    onChange={this.handleChangeAutomobile}
                                    value={this.state.automobile}
                                    required
                                    name="automobile"
                                    id="automobile"
                                    className="form-select"
                                >
                                    <option value="">
                                        Choose a Automobile
                                    </option>
                                    {this.state.automobiles.map(
                                        (automobile) => {
                                            return (
                                                <option
                                                    key={automobile.id}
                                                    value={automobile.vin}
                                                >
                                                    {automobile.vin}
                                                </option>
                                            );
                                        }
                                    )}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select
                                    onChange={this.handleChangeSalesperson}
                                    value={this.state.salesperson}
                                    required
                                    name="salesperson"
                                    id="salesperson"
                                    className="form-select"
                                >
                                    <option value="">
                                        Choose a Salesperson
                                    </option>
                                    {this.state.salespersons.map(
                                        (salesperson) => {
                                            return (
                                                <option
                                                    key={salesperson.id}
                                                    value={salesperson.id}
                                                >
                                                    {salesperson.name}
                                                </option>
                                            );
                                        }
                                    )}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select
                                    onChange={this.handleChangeCustomer}
                                    value={this.state.customer}
                                    required
                                    name="customer"
                                    id="customer"
                                    className="form-select"
                                >
                                    <option value="">Choose a Customer</option>
                                    {this.state.customers.map((customer) => {
                                        return (
                                            <option
                                                key={customer.id}
                                                value={customer.id}
                                            >
                                                {customer.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleChangePrice}
                                    value={this.state.price}
                                    placeholder="price"
                                    required
                                    type="Number"
                                    name="price"
                                    id="price"
                                    className="form-control"
                                />
                                <label htmlFor="price">Price</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
                <div className={messageClasses} id="success-message">
                    Congratulations on the Sale!
                </div>
            </div>
        );
    }
}
export default SalesForm;
