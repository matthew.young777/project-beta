import React from "react";

class TechnicianForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            employee_number: "",
            success: false,
        };
        this.handleChangeEmployeeNumber =
            this.handleChangeEmployeeNumber.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeEmployeeNumber(event) {
        const value = event.target.value;
        this.setState({ employee_number: value });
    }

    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.success;
        const techUrl = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(techUrl, fetchConfig);
        if (response.ok) {
            const newTech = await response.json();
            console.log(newTech);
            this.setState({
                name: "",
                employee_number: "",
                success: true,
            });
            await setTimeout(() => {
                // this.componentDidMount();
                window.location.reload();
            }, 2000);
        }
    }
    render() {
        let messageClasses = "alert alert-success d-none mb-0";
        if (this.state.success) {
            messageClasses = "alert alert-success mb-0";
        }
        return (
            <div className="row">
                <div className={messageClasses} id="success-message">
                    Technician Created!
                </div>
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Enter a technician</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-techician-form"
                        >
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleChangeName}
                                    value={this.state.name}
                                    placeholder="Name"
                                    required
                                    type="text"
                                    name="name"
                                    id="name"
                                    className="form-control"
                                />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleChangeEmployeeNumber}
                                    value={this.state.employee_number}
                                    placeholder="Employee Number"
                                    required
                                    type="text"
                                    name="employee_number"
                                    id="employee_number"
                                    className="form-control"
                                />
                                <label htmlFor="employee_number">
                                    Employee Number
                                </label>
                            </div>

                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default TechnicianForm;
