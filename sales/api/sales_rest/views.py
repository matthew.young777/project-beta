from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from django.http import JsonResponse

from .models import AutomobileVO, Customer, Sale, Salesperson

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin","import_href"]

# class VehicleVODetailEncoder(ModelEncoder):
#     model = VehicleVO
#     properties = ["name","import_href"]


class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = ["name",
    "number",
        "id"]

class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = ["name",
    "number"]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["name",
    "address",
    "phonenumber",
        "id"]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["name",
    "address",
    "phonenumber",]

class SalesListEncoder(ModelEncoder):
    model = Sale
    properties = ["price",
        "id"]
    def get_extra_data(self, o):
        return {"automobile": o.automobile.vin ,
        "salesperson": o.salesperson.name ,
        "customer": o.customer.name,
        "number": o.salesperson.number}

class SalesDetailEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile",
    "salesperson",
    "customer",
    "price"]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "salesperson": SalespersonDetailEncoder(),
        "customer": CustomerDetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_salespersons(request):

    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalespersonListEncoder
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_sales(request, pk=None):

    if request.method == "GET":
        if pk is not None:
            sales = Sale.objects.filter(salesperson=pk)
        else:
            sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            print("autoVO", AutomobileVO.objects)
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile id"},
                status=400,
            )
        try:
            salesperson_href = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_href)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=400,
            )
        try:
            customer_href = content["customer"]
            customer = Customer.objects.get(id=customer_href)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SalesDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):

    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )