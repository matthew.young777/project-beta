from django.contrib import admin
from django.urls import path, include

from .views import api_list_customers, api_list_sales, api_list_salespersons




urlpatterns = [
    path ("salespersons/", api_list_salespersons, name="api_list_salespersons"),
    path ("customers/", api_list_customers, name="api_list_customers"),
    path ("sales/", api_list_sales, name="api_list_sales"),
     path ("sales/<int:pk>", api_list_sales, name="api_list_specific_sales"),

]