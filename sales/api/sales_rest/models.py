from django.db import models
from django.urls import reverse
# Create your models here.

# class VehicleVO(models.Model):
#     name = models.CharField(max_length=100)

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True, null=True)
    import_href = models.CharField(max_length=300, null=True)


class Salesperson(models.Model):

    name = models.CharField(max_length=200, unique=True)
    number = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_list_salespersons", kwargs={"pk": self.pk})

    
class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phonenumber = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_list_customers", kwargs={"pk": self.pk})


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.PROTECT, blank=True)
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.PROTECT, blank=True)
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.PROTECT, blank=True)
    price = models.IntegerField(blank=True, null=True)

    def get_api_url(self):
        return reverse("api_list_sales", kwargs={"pk": self.pk})